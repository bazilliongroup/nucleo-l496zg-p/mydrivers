/*
Library:			LCD TFT EPD Driver for STM32 MCUs
Written by:			Ronald Bazillion
Date written:			10/08/2018
Description:			This is a 2.15  � a -Si, active matrix TFT, Electronic Paper Display (EPD) panel. The panel has such high resolution (110 dpi) 
                                that it is able to easily display fine patterns. This code is the driver code for the 2.14 LCD TFT Panel for STM32.	
Resources:
				http://www.pervasivedisplays.com/LiteratureRetrieve.aspx?ID=232067	
				http://www.pervasivedisplays.com/LiteratureRetrieve.aspx?ID=232068
                                http://en.radzio.dxp.pl/bitmap_converter/
                                https://www.youtube.com/watch?v=NUErX4dx2Tw&index=21&list=PLfExI9i0v1sn_lQjCFJHrDSpvZ8F2CpkA

Modifications:
-----------------

*/

//***** Header files *****//
#include <stdlib.h>
#include <string.h>
#include "LcdTftEpdDrv.h"
#include "LcdTftEpdFont.h"

//***** Defines ********//
//#define DEBUG

//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
static TFT_WIRES_TYPEDEF m_tftWireDef;
static SPI_HandleTypeDef* m_spiHdl;
static UART_HandleTypeDef* m_uartHdl;
static uint8_t m_rotation;

#ifdef DEBUG
static char m_txUartBuf[50];
#endif


//Image Buffer to be sent to the display
static uint8_t m_buffer[ MAX_SIZE ];
  
//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//
static void m_sendIndexData( uint8_t index, uint8_t *data, uint16_t len );
static void m_getFontDescriptors(const SYMBOL_NAME symbol, uint32_t* charWidth, uint32_t* charHeight, uint32_t* offset);
static void m_clearImage(uint8_t color);
static void m_loadImage( const uint8_t *buf, int len );
static void m_sendImage(void);

/*************************************************************************************************************************************/
/*****************************************************************API FUNCTIONS*******************************************************/
/*************************************************************************************************************************************/
/*************************************************Remember "API" is used like "public" in C++*****************************************/

void EPD215_init( SPI_HandleTypeDef* spiHdl, UART_HandleTypeDef* uartHdl, TFT_WIRES_TYPEDEF* tftWireDef)
{
    m_spiHdl = spiHdl;
    
#ifdef DEBUG    
    m_uartHdl = uartHdl;
#else
    m_uartHdl = NULL;
#endif    
    
    m_tftWireDef = *tftWireDef;
    m_rotation = 0;     //no rotation.
}

//Power on COGdriver
void EPD215_powerCogDriver(void)
{   
    HAL_GPIO_WritePin(m_tftWireDef.resetPort, m_tftWireDef.resetPin, GPIO_PIN_RESET);                   //RES# = 0
    HAL_Delay(50);                                                                                      //Delay 50ms
    HAL_GPIO_WritePin(m_tftWireDef.panelOnPort, m_tftWireDef.panelOnPin, GPIO_PIN_SET);                 //Panel On# = 1
    HAL_Delay(50);                                                                                      //Delay 50ms
    HAL_GPIO_WritePin(m_tftWireDef.resetPort, m_tftWireDef.resetPin, GPIO_PIN_SET);                     //RES# = 1
    HAL_Delay(1);                                                                                       //Delay 1ms
    HAL_GPIO_WritePin(m_tftWireDef.chipSelectPort, m_tftWireDef.chipSelectPin, GPIO_PIN_SET);           //CS# = 1
        
    while (HAL_GPIO_ReadPin(m_tftWireDef.busySelectPort, m_tftWireDef.busySelectPin) == GPIO_PIN_SET);  //Make sure BUSY = LOW
}

//Initialize COG Driver
void EPD215_initCogDriver(void)
{
    while (HAL_GPIO_ReadPin(m_tftWireDef.busySelectPort, m_tftWireDef.busySelectPin) == GPIO_PIN_SET);  //Make sure BUSY = LOW
    
    uint8_t data1[] = { 0xCF, 0x00 };
    m_sendIndexData( DRIVEROUTPUTCONTROL, data1, 2 );                      //Driver output control setting SPI(0x01,0xCF,0x00)
    uint8_t data2[] = { 0x00 };
    m_sendIndexData( GATESCANSTART, data2, 1 );                            //Gate scan start setting SPI(0x0F,0x00)
    uint8_t data3[] = { 0x03 };
    m_sendIndexData( DATAENTRYMODE, data3, 1 );                            //Data entry mode setting SPI(0x11,0x03)
    uint8_t data4[] = { 0x00, 0x0D };
    m_sendIndexData( SETRAMXSTARTEND, data4, 2 );                          //Set RAMX�Start / End position SPI(0x44,0x00,0x0D)
    uint8_t data5[] = { 0x00, 0xCF };
    m_sendIndexData( SETRAMYSTARTEND, data5, 2 );                          //Set RAMY�Start / End posistion SPI(0x45,0x00,0xCF)
    uint8_t data6[] = { 0x00 };
    m_sendIndexData( SETRAMXADDRESS, data6, 1 );                           //Set RAMX address counter SPI(0x4E,0x00)
    uint8_t data7[] = { 0x00 }; 
    m_sendIndexData( SETRAMYADDRESS, data7, 1 );                           //Set RAMY address counter SPI(0x4F,0x00)
}

//Update Command Sequence
void EPD215_updateCommandSequence(void)
{
    uint8_t data8[] = { 0x80 };
    m_sendIndexData( SETBORDERWAVEFORM, data8, 1 );                       //Select border waveform SPI(0x3C,0x80)
    uint8_t data9[] = { 0x10, 0x0A };
    m_sendIndexData( SETGATEDRIVINGVOLT, data9, 2 );                      //Set gate driving voltage SPI(0x03,0x10,0x0A)
    uint8_t data10[] = { 0x00 };
    m_sendIndexData( NORMALANALOGMODE1, data10, 1 );                      //Normal analog mode SPI(0x05,0x00)
    uint8_t data11[] = { 0x00, 0x00, 0x00 };
    m_sendIndexData( NORMALANALOGMODE2, data11, 3 );                      //Normal analog mode SPI(0x75,0x00,0x00,0x00)
}

//Send Teperature
void EPD215_sendTemperature(uint8_t temperature)
{
    uint8_t data12[] = { temperature, 0x00 };
    m_sendIndexData( SETTEMPERATURE, data12, 2 );                         //Send the temperature value SPI(0x1A,temperature,0x00)
    uint8_t data13[] = { 0xF7 };
    m_sendIndexData( SETUPDATESEQUENCE, data13, 1 );                      //Setup date sequence SPI(0x22,0xF7)
    uint8_t data14[] = { 0x00 };
    m_sendIndexData( ACTIVEDISPLAYUPDATE, data14, 1 );                    //Active display update sequence SPI(0x20, 0x00)
    
    //Update command sequence
    while (HAL_GPIO_ReadPin(m_tftWireDef.busySelectPort, m_tftWireDef.busySelectPin) == GPIO_PIN_SET);  //Make sure BUSY = LOW
}

//NOTE: invert inverts the image 
void EPD215_loadImage( uint8_t *buf, int len, bool invert )
{
  if (invert)
  {
    for (int i=0; i<len; i++)
    {
      buf[i] = ~buf[i];
    }
  }
  m_loadImage( buf, len );
}

void EPD215_sendImage(void)
{
  m_sendImage();
}

// Clear Screen
void EPD215_clearScreen(uint8_t color)
{
  m_clearImage(color);
  m_sendImage();
}

// Send Pattern
void EPD215_sendPattern( uint8_t pattern)
{
  //Load pattern
  for ( int i = 0; i < MAX_SIZE; i++ ) 
  {
    m_buffer[ i ] = pattern;
  }
  m_sendImage();
}

// Send Character
void EPD215_sendCharacter(const SYMBOL_NAME symbol)
{
  volatile int i = 0;
  
  uint32_t charWidth; 
  uint32_t charHeight; 
  uint32_t offset;
  
  m_getFontDescriptors( symbol, &charWidth, &charHeight, &offset );
    
  //Load image
  for (i = 0; i < charHeight; i++ ) 
  {
    m_buffer[ i ] = fontBitmaps[ offset ];
  }
  
  m_sendImage();
}

// the most basic function, set a single pixel
// drawPixel function cribbed from adafruit gfx library
void EPD215_drawPixel( int16_t x, int16_t y, uint16_t color )
{
  uint32_t t;
  uint8_t *ptr;
  
  if (( x < 0 ) || ( y < 0 ) || ( x >= WIDTH ) || ( y >= HEIGHT )) 
  {
    return;
  }
  
  //t is temporary
  switch ( m_rotation ) {
    case 1:
      t = x;
      x = WIDTH  - 1 - y;
      y = t;
      break;
    case 2:
      x = WIDTH  - 1 - x;
      y = HEIGHT - 1 - y;
      break;
    case 3:
      t = x;
      x = y;
      y = HEIGHT - 1 - t;
      break;
  }
  
  ptr = &m_buffer[( x / 8 ) + y * (( WIDTH + 7 ) / 8 )];
  
  if ( color ) 
  {
    *ptr |= 0x80 >> ( x & 7 );
  }
  else         
  {
    *ptr &= ~( 0x80 ) >> ( x & 7 );
  }
}

/*************************************************************************************************************************************/
/****************************************************************STATIC FUNCTIONS*****************************************************/
/*************************************************************************************************************************************/

//NOTE: len is the length of the data.
static void m_sendIndexData( uint8_t index, uint8_t *data, uint16_t len )
{    
    HAL_GPIO_WritePin(m_tftWireDef.chipSelectPort, m_tftWireDef.chipSelectPin, GPIO_PIN_RESET);                 //CS# Low
    HAL_GPIO_WritePin(m_tftWireDef.dataCommandPort, m_tftWireDef.dataCommandPin, GPIO_PIN_RESET);               //DC# Low    
    HAL_Delay(1);
  
    HAL_SPI_Transmit(m_spiHdl, &index, 1, 10);                                                               //SPI Send Index
  
    HAL_Delay(1);
    HAL_GPIO_WritePin(m_tftWireDef.dataCommandPort, m_tftWireDef.dataCommandPin, GPIO_PIN_SET);               //DC# High
    HAL_Delay(1);
  
    HAL_SPI_Transmit(m_spiHdl, &data[0], len, 10);                                                        //SPI Send Index
  
    HAL_Delay(1);  
    HAL_GPIO_WritePin(m_tftWireDef.chipSelectPort, m_tftWireDef.chipSelectPin, GPIO_PIN_SET);                 //CS# High
}

static void m_clearImage(uint8_t color)
{
  
  //set to black color
  if  (color == BLACK)
  {
      color = 0xFF;
  }
  else if (color == WHITE)
  {
      color = 0x00;
  }
  else
  {
      return;
  }
 
  //0x00 - white color, 0xFF - black color
  for ( int i = 0; i < MAX_SIZE; i++ ) 
  {
    m_buffer[ i ] = color;
  }
}

static void m_getFontDescriptors(const SYMBOL_NAME symbol, uint32_t* charWidth, uint32_t* charHeight, uint32_t* offset)
{    
    *charWidth = fontDescriptors[symbol][0];
    *charHeight = fontDescriptors[symbol][1];  
    *offset = fontDescriptors[symbol][2];
}

// Write To Buffer
static void m_loadImage( const uint8_t *buf, int len )
{
  for ( int i = 0; i < len; i++ ) 
  {
    m_buffer[ i ] = buf[ i ];
  }
}

//Send Image Data
static void m_sendImage(void)
{
  //Send Image Data
  m_sendIndexData( SENDIMAGEDATA, m_buffer, MAX_SIZE );
  uint8_t data13[] = { 0xF7 };
  m_sendIndexData( SETUPDATESEQUENCE, data13, 1 );                      //Setup update sequence SPI(0x22,0xF7)
  uint8_t data14[] = { 0x00 };
  m_sendIndexData( ACTIVEDISPLAYUPDATE, data14, 1 );                    //Active display update sequence SPI(0x20)*3
  
  //Update command sequence
  while (HAL_GPIO_ReadPin(m_tftWireDef.busySelectPort, m_tftWireDef.busySelectPin) == GPIO_PIN_SET);  //Make sure BUSY = LOW
}